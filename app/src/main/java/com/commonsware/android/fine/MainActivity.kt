package com.commonsware.android.fine

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.commonsware.android.fine.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val permRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            populateLabels()
        }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        populateLabels()
    }

    private fun populateLabels() {
        binding.finePermission
            .setText(
                hasPerm(ACCESS_FINE_LOCATION)
                    .either(
                        R.string.fine_permission,
                        R.string.no_fine_permission
                    )
            )
        binding.coarsePermission
            .setText(
                hasPerm(ACCESS_COARSE_LOCATION)
                    .either(
                        R.string.coarse_permission,
                        R.string.no_coarse_permission
                    )
            )
        binding.fineRationale
            .setText(
                shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)
                    .either(
                        R.string.fine_rationale,
                        R.string.no_fine_rationale
                    )
            )
        binding.coarseRationale
            .setText(
                shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)
                    .either(
                        R.string.coarse_rationale,
                        R.string.no_coarse_rationale
                    )
            )

        binding.requestPermission.setOnClickListener {
            permRequest.launch(ACCESS_FINE_LOCATION)
        }
    }

    private fun hasPerm(perm: String) =
        checkSelfPermission(perm) == PackageManager.PERMISSION_GRANTED

    private fun <T> Boolean.either(foo: T, bar: T): T = if (this) foo else bar
}