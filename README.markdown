This project illustrates the behavior of Android &mdash; particularly Android 12 &mdash;
when we request fine location permission but the user only grants coarse location permission.

See [this Stack Overflow question](https://stackoverflow.com/q/69530502/115145) for more
context around this project.
